import React from 'react'
import {useState,useEffect} from 'react';
import MaterialTable from 'material-table';
import Input from './Input';

const ChargeRecrutement = () => {
    const [B,setB]=new useState([]);
    const [Type,setType]=new useState('Formation');
    const [D,setD]=new useState([]);
    const [Info,setInfo]=new useState('Non Confirmé');
    useEffect(()=>{
        const A = fetch("http://127.0.0.1:8000/api/t/d");
        A.then(e=>e.json()).then(e=>{setB(e)});
        const C = fetch("http://127.0.0.1:8000/api/t/a");
        C.then(e=>e.json()).then(e=>{setD(e)});
    },[]);
    const data=B.map(e=>e);
    const columns=[
        {
          title:'Nom',field:'Nom'
        },
        {
          title:'Prénom',field:'Prénom'
        },
        {
          title:'Email' ,field:'Email'
        },
        {
          title:'Tél' , field:'Tél'
        },
       
    ]
    const donnee=D.map(e=>e);
    const col=[
        {
            title:'Libellé',field:'Libellé'
          },
          {
            title:'DateDebut',field:'DateDebut'
          },
          {
            title:'DateFin' ,field:'DateFin'
          },
    ]
    const Table=()=>{
      setType('Candidat');
    }
    const Edit=()=>{
        setType('Formation');
    }
    const update=()=>{
        setInfo('Confirme');
    }
    return (
        <div>
            <div className="sidebar">
              <a className="active" href="#home"onClick={Edit}>Formation</a>
              <a href="#news" onClick={Table}>Candidat</a>
            </div>
            <div className="content">
            <h2>Responsive Sidebar Example</h2>
            <form >
             {/* <MaterialTable title='Liste des candidat' data={data} columns={columns}/> */}
             {
                 Type==='Formation'?(<MaterialTable title='Liste des formations' data={donnee} columns={col} />):(<MaterialTable title='Liste des candidat' data={data} columns={columns} options={{actionsColumnIndex: -1}} actions={[
                    {
                      icon: 'save',
                      tooltip: 'Save User',
                      onClick: (event, rowData) => {
                        update();
                      }
                    }
                  ]}/>)
             }
            </form>
         </div>
         <Input Info={Info}/>
        </div>
    )
}

export default ChargeRecrutement
