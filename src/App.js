import logo from './logo.svg';
import './App.css';
import {BrowserRouter, BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import Logins from './components/Login/Logins';
import ChefPlateu from './components/ChefPlateu';
import ChargeRecrutement from './components/ChargeRecrutement';
import Formatrice from './components/Formatrice';
import Table from './components/Table';
function App() {
  
  return (
    
    <div className="App">
      <BrowserRouter>
      <Router>
        <Switch>
          <Route  exact path='/ChefPlateau' component={ChefPlateu}/>
          <Route  exact path='/' component={Logins}/>
          <Route  exact path='/ChargeRecrutement' component={ChargeRecrutement}/>
          <Route  exact path='/Formatrice' component={Formatrice}/>
          <Route  exact path='/Table' component={Table}/>
        </Switch>
      </Router>
    </BrowserRouter>
    </div>
  );
}

export default App;
